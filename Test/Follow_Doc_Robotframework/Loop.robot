*** Settings ***
Library           Collections
Library           Selenium2Library

*** Variables ***
@{LIST}           a    b    c
@{NUMBERS}        ${1}    ${2}    ${5}
@{NAMES}          one    two    five
${var}            0
${user}           watcharapong.w
${pass}           HYOUNGv2535s

*** Test Cases ***
Three loop variables
    [Tags]    Loop    Thee_Val    V3.1.1
    @{List}    Create List
    @{EMPLOYERS}    Create List
    : FOR    ${index}    ${english}    ${finnish}    IN    1    cat
    ...    kissa    2    dog    koira    3    horse
    ...    hevonen
    \    Log Many    ${english}    ${finnish}    ${index}
    \    Collections.Append To List    ${List}    ${english}    ${finnish}    ${index}
    \    Log    ${List}
    : FOR    ${name}    ${id}    ${ind}    IN    @{List}
    \    Log Many    ${ind} + ${name} + ${id}

Only upper limit
    [Documentation]    Loops over values from 0 to 9
    : FOR    ${index}    IN RANGE    10
    \    Log    ${index}

Start and end
    [Documentation]    Loops over values from 1 to 10
    : FOR    ${index}    IN RANGE    1    11
    \    Log    ${index}

Also step given
    [Documentation]    Loops over values 5, 15, and 25
    : FOR    ${index}    IN RANGE    5    45    10
    \    Log    ${index}

Negative step
    [Documentation]    Loops over values 13, 3, and -7
    : FOR    ${index}    IN RANGE    13    -13    -10
    \    Log    ${index}

Arithmetic
    [Documentation]    Arithmetic with variable
    : FOR    ${index}    IN RANGE    ${var} + 1
    \    Log    ${index}

Float parameters
    [Documentation]    Loops over values 3.14, 4.34, and 5.54
    : FOR    ${index}    IN RANGE    3.14    6.09    1.2
    \    Log    ${index}

Manage index manually
    ${index} =    Set Variable    -1
    : FOR    ${item}    IN    @{LIST}
    \    Log    ${index} + 1
    \    ${index} =    Evaluate    ${index} + 1
    \    log many    ${index}    ${item}

For-in-enumerate
    Comment    abc
    : FOR    ${index}    ${item}    IN ENUMERATE    @{LIST}
    \    Log many    ${index}    ${item}

For-in-enumerate with two values per iteration
    : FOR    ${index}    ${en}    ${fi}    IN ENUMERATE    cat    kissa
    ...    dog    koira    horse    hevonen
    \    Log    "${en}" in English is "${fi}" in Finnish (index: ${index})

Iterate over two lists manually
    ${length}=    Get Length    ${NUMBERS}
    : FOR    ${idx}    IN RANGE    ${length}
    \    log many    ${NUMBERS}[${idx}]    ${NAMES}[${idx}]

For-in-zip
    : FOR    ${number}    ${name}    IN ZIP    ${NUMBERS}    ${NAMES}
    \    log many    ${number} + ${name}

Exit Example
    ${text} =    Set Variable    ${EMPTY}
    : FOR    ${var}    IN    one    two
    \    Run Keyword If    '${var}' == 'two'    Exit For Loop
    \    ${text} =    Set Variable    ${text}${var}
    Should be equal    ${text}    one

Continue Example
    ${text} =    Set Variable    ${EMPTY}
    : FOR    ${var}    IN    one    two    three
    \    Log    Index : ${var}
    \    Continue For Loop If    '${var}' == 'two'
    \    Log    Index_2 : ${var}
    \    Log    Sum \ : ${text}${var}
    \    ${text} =    Set Variable    ${text}${var}
    Should Be Equal    ${text}    onethree

Selendroid

TimeSheets
    Open Browser    https://newtimesheet.aware.co.th/timesheet/Login.aspx
    Maximize Browser Window
    Wait Until Page Contains Element    //*[@id="cphContent_txtUserName"]
    Wait Until Page Contains Element    //*[@id="cphContent_txtUserPassword"]
    Wait Until Page Contains Element    //*[@id="cphContent_btnLogin"]
    Input Text    //*[@id="cphContent_txtUserName"]    ${user}
    Input Text    //*[@id="cphContent_txtUserPassword"]    ${pass}
    Click Element    //*[@id="cphContent_btnLogin"]
    Capture Page Screenshot
    Close All Browsers

*** Keywords ***
Handle Table
    [Arguments]    @{table}
    : FOR    ${row}    IN    @{table}
    \    Handle Row    @{row}

Handle Row
    [Arguments]    @{row}
    : FOR    ${cell}    IN    @{row}
    \    Handle Cell    ${cell}
