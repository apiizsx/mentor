import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact';
import { TabsPage } from '../tabs/tabs';
import {SignInPage} from '../sign-in/sign-in';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tab1Root = TabsPage;
  tab2Root = AboutPage;
  tab3Root = SignInPage;

  constructor() {

  }
}
