//import { StaffHomePage } from './../staff-home/staff-home';
//import { MenuItem } from './../../app/_centralized/_models/_menu-item/menu-item';//
//import { TabsPage } from '../pages/tabs/tabs';
import { User } from './../../app/_centralized/_models/_users/user';
import { Profile } from './../../app/_centralized/_models/_users/profile';
import { AuthenticateUsersProvider } from '../../providers/authenticate-users/authenticate-users';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { GENARAL } from '../../app/_centralized/_configuration/genaral';
import { Subscription } from 'rxjs';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { Status } from '../../app/_centralized/_models/_status/status';
import { DatasMenu } from '../../app/_centralized/_models/_menu/datasMenu';
/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  sigInForm: FormGroup;
  _GENARAL: any;
  userProfile: Profile;

  datasMenu: DatasMenu[];
  menu: DatasMenu;
  dataResults: any;

  userType: string = '';
  username: string = '';
  password: string = '';

  constructor(private navController: NavController,
    private formBuilder: FormBuilder,
    private authenticateUsers: AuthenticateUsersProvider,
    private manageStorage: ManageStorageProvider,
    public alerCtrl: AlertController,
    private realtimeDatabase: RealtimeDatabaseProvider,
    private toastCtrl: ToastController) {

    this.sigInForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this._GENARAL = GENARAL;

  }

  private onErrorValid(): void {
    if (!this.sigInForm.controls.username.valid) {
      this.showToast(this._GENARAL.TOAST.POSITION_BOTTOM, this._GENARAL.ERROR.REQUIRE_LOGIN_USERNAME);
    } else if (!this.sigInForm.controls.password.valid) {
      this.showToast(this._GENARAL.TOAST.POSITION_BOTTOM, this._GENARAL.ERROR.REQUIRE_LOGIN_PASSWORD);
    }
  }

  ionViewDidLoad(): void {
    console.log('ionViewDidLoad SignInPage');
  }

  private callBackError(error: any): void {
    console.log(error);
    this.showToast(this._GENARAL.TOAST.POSITION_MIDDLE, this._GENARAL.ERROR.LOGIN_FAILED);
    this.manageStorage.clearStorage();
  }

  changePageToSignUp(): void {
    this.navController.setRoot(SignUpPage, {}, { animate: true, direction: 'forward' });
  }

  onSubmitSignIn(): void {
    if (this.sigInForm.valid) {
      this.authenticateUsers.signInWithUsernameAndPassword(this.sigInForm.controls.username.value,
        this.sigInForm.controls.password.value).then((userCredential: firebase.auth.UserCredential) => {
          console.log('Sign In Success: ', userCredential);
          this.getUserProfile(userCredential.user.uid);
        }).catch(this.callBackError.bind(this));
    } else {
      this.onErrorValid();
    }
  }
  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'none';
        });
    }
}
  private getUserProfile(uidUser: string): void {
    let readOnce: Subscription = this.realtimeDatabase.readProfileResult(uidUser).subscribe((results: any) => {
      readOnce.unsubscribe();
      console.log('results.payload.val() = ', results.payload.val());
      const user: User = results.payload.val();
      this.userProfile = {
        username: user.profile.username,
        name: user.profile.name,
        address: user.profile.address,
        telephone: user.profile.telephone,
        userType: user.profile.userType
      }
      console.log('- - - - - - - - - - - - - - -');
      console.table(this.userProfile);
      console.log(uidUser);
      console.log(uidUser);
      console.log('- - - - - - - - - - - - - - -');
      this.manageStorage.setUserProfile(this.userProfile);
      this.manageStorage.setUidUser(uidUser);
      // this.getStatusDetails(this.userProfile.userType);
      this.showToast(this._GENARAL.TOAST.POSITION_BOTTOM, this._GENARAL.TOAST.LOGIN_SUCCESS);
      setTimeout(() => {
        if (this.userProfile.userType === 'C') {
          this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
        } else if (this.userProfile.userType === 'S') {
        //  this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
      }
      }, 500);
    }, this.callBackError.bind(this));
  }

  private getStatusDetails(userType: string): void {
    console.log('.:: getStatusDetails ::.');
    let readOnce: Subscription = this.realtimeDatabase.readStatusResult().subscribe((results: any) => {
      readOnce.unsubscribe();
      const status: Status = results.payload.val();
      this.manageStorage.setStatus(status);
      this.showToast(this._GENARAL.TOAST.POSITION_BOTTOM, this._GENARAL.TOAST.LOGIN_SUCCESS);
      setTimeout(() => {
        if (userType === 'C') {
          this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
        //} else if (userType === 'S') {
          //this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
       }
      }, 500);
    }, this.callBackError.bind(this));
  }

  private showToast(position: string, message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: position
    });
    toast.present();
  }
}