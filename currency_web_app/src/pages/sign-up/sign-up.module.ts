import { GENARAL } from '../../app/_centralized/_configuration/genaral';
import { AuthenticateUsersProvider } from '../../providers/authenticate-users/authenticate-users';
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { SignInPage } from '../sign-in/sign-in';
import { User } from '../../app/_centralized/_models/_users/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage implements OnInit {

  singUpForm: FormGroup;
  username: string = '';
  namelassname: string = '';
  address: string = '';
  password: string = '';
  rePassword: string = '';
  telephone: string = '';
  user: User;
  _GENARAL: any;

  constructor(private navController: NavController,
    private authenticateUsers: AuthenticateUsersProvider,
    private realtimeDatabase: RealtimeDatabaseProvider,
    public formBuilder: FormBuilder,
    public alerCtrl: AlertController) { }

  ngOnInit(): void {
    this.singUpForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9]+'), Validators.minLength(5), Validators.maxLength(30)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      repassword: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      name: ['', Validators.compose([Validators.required])],
      address: ['', [Validators.required]],
      telephone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });

    this._GENARAL = GENARAL;
  }

  ionViewDidLoad(): void {
    console.log('ionViewDidLoad SignUpPage');
  }

  private createUser(username: string, password: string, user: User): void {
    this.authenticateUsers.createUserWithUsernameAndPassword(username, password)
      .then((userCredential: firebase.auth.UserCredential) => {
        let objectCreateUser: object = {};
        console.log('userCredential.user.uid : ', userCredential.user.uid);
        objectCreateUser[userCredential.user.uid] = user;
        console.log('user : ', user);
        console.log('objectCreateUser : ', objectCreateUser);
        this.realtimeDatabase.saveInformationUser(objectCreateUser).then(() => {
          console.log('Create User Success: ', objectCreateUser);
          this.changePageToSingIn();
        }).catch(this.callBackError.bind(this));
      }).catch(this.callBackError.bind(this));
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showAlert(this._GENARAL.ALERT.ERROR, 'ไม่สามารถดำเนินการได้');
  }

  changePageToSingIn(): void {
    this.navController.setRoot(SignInPage, {}, { animate: true, direction: 'back' });
  }

  onSubmitSignUp(): void {
    console.log('.:: onSubmitSignUp ::.');
    if (this.singUpForm.valid && (this.password === this.rePassword)) {
      this.user = {
        profile: {
          username: this.singUpForm.controls.username.value,
          address: this.singUpForm.controls.address.value,
          name: this.singUpForm.controls.name.value,
          telephone: this.singUpForm.controls.telephone.value,
          userType: 'C'
        },
        referenceOrderItem: null
      }

      this.createUser(this.singUpForm.controls.username.value, this.singUpForm.controls.password.value, this.user);
    } else {
      this.onErrorValid();
    }
  }

  showAlert(title: string, message: string) {
    let alert = this.alerCtrl.create({
      title: title,
      message: message,
      buttons: ['ตกลง']
    });
    alert.present()
  }

  private onErrorValid(): void {
    const title = 'คำเตื่อน';
    if (!this.singUpForm.controls.username.valid) {
      console.log(this.singUpForm.controls.username.value);
      const username: string = this.singUpForm.controls.username.value;
      if (username === null || username === '') {
        this.showAlert(title, 'กรุณากรอกข้อมูล "ชื่อผู้ใช้งาน"');
      } else if (username.length < 5) {
        this.showAlert(title, 'โปรดระบุชื่อผู้ใช้งาน ความยาวไม่น้อยกว่า 5 ตัวอักษร');
      } else {
        this.showAlert(title, 'ข้อมูล "ชื่อผู้ใช้งาน" ผิดพลาด กรุณากรอกใหม่');
      }
    } else if (!this.singUpForm.controls.name.valid) {
      this.showAlert(title, 'กรุณากรอกข้อมูล "ชื่อ-นามสกุล"');
    } else if (!this.singUpForm.controls.address.valid) {
      this.showAlert(title, 'กรุณากรอกข้อมูล "ที่อยู่"');
    } else if (!this.singUpForm.controls.password.valid) {
      if (this.password === null || this.password === '') {
        this.showAlert(title, 'กรุณากรอกข้อมูล "รหัสผ่าน"');
      } else {
        this.showAlert(title, 'ข้อมูล "รหัสผ่าน" ผิดพลาด กรุณากรอกใหม่');
      }
    }
    else if (!this.singUpForm.controls.repassword.valid) {
      if (this.rePassword === null || this.rePassword === '') {
        this.showAlert(title, 'กรุณากรอกข้อมูล "ยืนยันรหัสผ่าน"');
      } else {
        this.showAlert(title, 'ข้อมูล "ยืนยันรหัสผ่าน" ผิดพลาด กรุณากรอกใหม่');
      }
    } else if (!this.singUpForm.controls.telephone.valid) {
      if (this.telephone === null || this.telephone === '') {
        this.showAlert(title, 'กรุณากรอกข้อมูล "เบอร์โทร"');
      } else {
        this.showAlert(title, 'ข้อมูล "เบอร์โทร" ผิดพลาด กรุณากรอกใหม่');
      }
    } else if (this.singUpForm.controls.password.valid && this.singUpForm.controls.repassword.valid) {
      if (this.singUpForm.controls.password.value !== this.singUpForm.controls.repassword.value) {
        this.showAlert(title, 'กรุณากรอกข้อมูล "รหัสผ่าน" และ "ยืนยันรหัสผ่าน" ให้ตรงกัน');
      }
    }
  }
}
