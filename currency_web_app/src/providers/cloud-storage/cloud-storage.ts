import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Injectable } from '@angular/core';

declare let unescape: Function;

@Injectable()
export class CloudStorageProvider {

  constructor(private angularFireStorage: AngularFireStorage) {
    console.log('Hello CloudStorageProvider Provider');
  }

  base64UrlToBlob(base64Url: string): Blob {
    let byteString;
    if (base64Url.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(base64Url.split(',')[1]);
    else
      byteString = unescape(base64Url.split(',')[1]);
    let mimeString = base64Url.split(',')[0].split(':')[1].split(';')[0];
    let uint8Array = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      uint8Array[i] = byteString.charCodeAt(i);
    }
    return new Blob([uint8Array], { type: mimeString });
  }

  storageReference(pathFileName: string): AngularFireStorageReference {
    return this.angularFireStorage.ref(pathFileName);
  }

  uploadFile(pathFileName: string, blobFile: Blob): AngularFireUploadTask {
    return this.angularFireStorage.upload(pathFileName, blobFile);
  }

}
