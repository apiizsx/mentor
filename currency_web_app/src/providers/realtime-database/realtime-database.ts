import { AngularFireDatabase, SnapshotAction } from '@angular/fire/database';
import { FIREBASE_PRODUCTS } from '../../app/_centralized/_configuration/firebase-products';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class RealtimeDatabaseProvider {

  constructor(private angularFireDatabase: AngularFireDatabase) { }

  readMenuResult(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.MENU_ITEM).snapshotChanges();
  }

  saveInformationUser(user: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.USERS).update(user);
  }

  readProfileResult(uidUser: string): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.USERS + '/' + uidUser).snapshotChanges();
  }

  saveOrUpdateOrderItem(orderItem: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM).update(orderItem);
  }

  readOrderItemResult(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM).snapshotChanges();
  }

  readStatusResult(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.STATUS).snapshotChanges();
  }

}
