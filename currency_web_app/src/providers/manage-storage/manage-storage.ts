import { GENARAL } from '../../app/_centralized/_configuration/genaral';
import { Injectable } from '@angular/core';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { DatasMenu } from '../../app/_centralized/_models/_menu/datasMenu';
import { Status } from '../../app/_centralized/_models/_status/status';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';

@Injectable()
export class ManageStorageProvider {

  setUidUser(uid: string): void {
    localStorage.setItem(GENARAL.LOCALSTORAGE.UID_USER, uid);
  }

  getUidUser(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.UID_USER);
  }

  addListOrder(order: ListOrder): void {
    console.log('.:: addListOrder ::. order = ', order);
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getListOrder();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menus of tempNewList) {
        if (menus.name !== order.name) {
          newList.push(menus);
        }
      }
      const amount: string = order.amount.toString();
      console.log('amount = ', amount);
      if (order.amount === 0 || amount === '0') {
        this.removeListOrder(order);
      } else {
        newList.push(order);
      }

    } else {
      newList.push(order);
    }
    localStorage.setItem(GENARAL.LOCALSTORAGE.LIST_ORDER, JSON.stringify(newList));

  }

  getListOrder(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.LIST_ORDER);
  }

  removeOrderImListOrder(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.LIST_ORDER);
  }

  removeListOrder(order: ListOrder): void {
    console.log('.:: removeListOrder ::. order = ', order);

    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getListOrder();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menus of tempNewList) {
        if (menus.name !== order.name) {
          newList.push(menus);
        }
      }
    }

    if (0 <= newList.length) {
      this.removeOrderImListOrder();
    } else {
      localStorage.setItem(GENARAL.LOCALSTORAGE.LIST_ORDER, JSON.stringify(newList));
    }
  }

  removeUidUser(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.UID_USER);

  }

  updateUidUser(uid: string): void {
    this.removeUidUser();
    this.setUidUser(uid);
  }

  clearStorage(): void {
    localStorage.clear();
  }

  setUserName(username: string): void {
    localStorage.setItem(GENARAL.LOCALSTORAGE.USER_NAME, username);
  }

  getUsername(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.USER_NAME);
  }

  removeUsername(username: string): void {
    localStorage.removeItem(username);
    this.setUserName(username);
  }

  getUserProfile(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.USER_PROFILE);
  }

  setUserProfile(user: Profile): void {
    console.log('.:: setUserProfile ::.');
    console.log('user = ', user);
    localStorage.setItem(GENARAL.LOCALSTORAGE.USER_PROFILE, JSON.stringify(user));
  }

  updateUserProfile(user: Profile): void {
    this.removeUserProfile();
    this.setUserProfile(user);
  }

  removeUserProfile(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.USER_PROFILE);
  }


  getOrderItem(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.ORDER_ITEM);
  }

  removeOrderItem(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.ORDER_ITEM);
  }

  addOrderItem(order: DetailOrder): void {
    console.log('.:: addOrderItem ::. order = ', order);

    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getOrderItem();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let orderOld of tempNewList) {
        newList.push(orderOld);
      }
      newList.push(order);

    } else {
      newList.push(order);
    }
    localStorage.setItem(GENARAL.LOCALSTORAGE.ORDER_ITEM, JSON.stringify(newList));

  }

  setStatus(status: Status): void {
    console.log('.:: setStatus ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getStatus();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let item of tempNewList) {
        newList.push(item);
      }
      newList.push(status);

    } else {
      newList.push(status);
    }
    localStorage.setItem(GENARAL.LOCALSTORAGE.STATUS, JSON.stringify(newList));
  }

  getStatus(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.STATUS);
  }

  removeStatus(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.STATUS);
  }

  getDatasMenu(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.MENU);
  }

  setDatasMenu(datasMenu: DatasMenu): void {
    console.log('.:: setDatasMenu ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getDatasMenu();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menu of tempNewList) {
        newList.push(menu);
      }
      newList.push(datasMenu);

    } else {
      newList.push(datasMenu);
    }
    localStorage.setItem(GENARAL.LOCALSTORAGE.MENU, JSON.stringify(newList));
  }

  setOrderItemFromDB(order: DetailOrder): void {
    console.log('.:: setOrderItemFromDB ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getOrderItemFromDB();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let item of tempNewList) {
        newList.push(item);
      }
      newList.push(order);

    } else {
      newList.push(order);
    }
    localStorage.setItem(GENARAL.LOCALSTORAGE.ORDER_ITEM, JSON.stringify(newList));
  }

  getOrderItemFromDB(): string {
    console.log('.:: setOrderItemFromDB ::.');
    return localStorage.getItem(GENARAL.LOCALSTORAGE.ORDER_ITEM);
  }

  getDeliveryDetailsOrder(): string {
    return localStorage.getItem(GENARAL.LOCALSTORAGE.DELIVERY_INVOICE_DETAILS);
  }

  updateDeliveryDetailsOrder(obj: any): void {
    console.log('.:: updateDeliveryDetailsOrder ::.');
    this.removeDeliveryDetailsOrder();
    this.setDeliveryDetailsOrder(obj);
  }

  setDeliveryDetailsOrder(obj: any): void {
    console.log('.:: setDeliveryDetailsOrder ::.');
    console.log('obj = ', obj);
    localStorage.setItem(GENARAL.LOCALSTORAGE.DELIVERY_INVOICE_DETAILS, JSON.stringify(obj));
  }
  removeDeliveryDetailsOrder(): void {
    localStorage.removeItem(GENARAL.LOCALSTORAGE.DELIVERY_INVOICE_DETAILS);
  }

}
