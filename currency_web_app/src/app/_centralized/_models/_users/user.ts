import { ReferenceOrderItem } from './referenceOrderItem';
import { Profile } from "./profile";

export interface User {
    profile: Profile;
    referenceOrderItem: ReferenceOrderItem;
}
