import { ListOrder } from './list-order';
import { SendContact } from './send-contact';

export interface DetailOrder {
    listOrder: ListOrder[];
    sendContact: SendContact;
    status: string;
    totalPrice: number;
    staff: string;
}
