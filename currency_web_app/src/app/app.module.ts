
import { MyApp } from './app.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthenticateUsersProvider } from '../providers/authenticate-users/authenticate-users';
import { BrowserModule } from '@angular/platform-browser';
import { CloudStorageProvider } from '../providers/cloud-storage/cloud-storage';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ManageStorageProvider } from '../providers/manage-storage/manage-storage';
import { ReactiveFormsModule } from '@angular/forms';
import { RealtimeDatabaseProvider } from '../providers/realtime-database/realtime-database';
import {SignUpPage} from '../pages/sign-up/sign-up';
import { SignInPage } from '../pages/sign-in/sign-in';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { ExchangeServiceProvider } from '../providers/exchange-service/exchange-service';
import { FIREBASE_PRODUCTS } from './_centralized/_configuration/firebase-products';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    SignInPage,
    HomePage,
    SignUpPage,
    TabsPage
  ],
  
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(FIREBASE_PRODUCTS.VERIFY_KEY),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    SignInPage,
    SignUpPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ExchangeServiceProvider,
    AuthenticateUsersProvider,
    RealtimeDatabaseProvider,
    CloudStorageProvider,
    ManageStorageProvider
  ]
})
export class AppModule {}
